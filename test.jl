"""
    myfunc(x::Integer) -> Integer

Returns an `Integer`

# Arguments

* 'x::Integer': an `Integer`
"""
function myfunc(x::Integer)::Integer
    d = Dict("x" => x)
    return d["x"] * 2
end

"""
    yourfunc(x::Integer) -> Integer

Returns an `Integer`

# Arguments

* 'x::Integer': an `Integer`
"""
function yourfunc(x::Integer)::Integer
    d = Dict("x" => x)
    return d["x"] * 2
end
